import React from 'react'
import{View,Text,TouchableOpacity} from 'react-native';

export default function ErrorPage(props){
    return (
        <View>
            <Text>Your internet probably is disconnected. Please re-connect and try again</Text>
            <Text>Go back to </Text><TouchableOpacity onPress={()=>{props.navigation.goBack()}}><Text>Home</Text></TouchableOpacity>
        </View>
    )
}