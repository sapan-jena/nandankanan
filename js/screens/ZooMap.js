import React,{Component} from 'react';
import { StyleSheet, View, Dimensions,TouchableOpacity,StatusBar,BackHandler,Linking,Image} from 'react-native';
import {Container,Content,Header,Text,Spinner, Icon, Grid,Left,Body,Right,Card,CardItem,Thumbnail } from 'native-base';

import BotanicalMap from '../../images/botanical-map.jpg'
import Guide from '../../images/nw-map.jpg';

import NHeader from './components/header'
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class ZooMap extends Component{
    constructor(props){
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }
    onBackPress = () => {
        this.props.navigation.navigate("Landing");
        return true;
    };
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
    }

    render(){
        return(
            <Container>
                <NHeader onPressHome={()=>this.props.navigation.navigate('Landing')} 
                    name="ZOO MAP"
                />
                
                
                <StatusBar barStyle="light-content"  backgroundColor="#0EA697" />
                <Content style={{paddingTop:10}}>
                    {/* <Text style={{marginTop:10,marginBottom:10,textAlign:'center',color:'#fff',fontSize:18,fontWeight:'bold',backgroundColor:'green',padding:10}}>ZOO MAP</Text> */}
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ZoologicalPark')}>
                        <Card style={{marginLeft:10,marginRight:10}}>
                        <CardItem header style={{alignSelf:'center'}}>
                                <Text style={{color:'#0EA697',fontWeight:'bold',fontSize:20}}>
                                    Zoological Park
                                </Text>
                            </CardItem>
                            <CardItem cardBody>
                                <Image source={Guide} style={{height: 200, width: null, flex: 1}}/>
                            </CardItem>
                            
                        </Card>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('BotanicalGarden')}>
                        <Card style={{marginLeft:10,marginRight:10}}>
                        <CardItem header style={{alignSelf:'center'}}>
                                <Text style={{color:'#0EA697',fontWeight:'bold',fontSize:20}}>
                                    Botanical Garden
                                </Text>
                            </CardItem>
                            <CardItem cardBody>
                                <Image source={BotanicalMap} style={{height: 200, width: null, flex: 1}}/>
                            </CardItem>
                            
                        </Card>
                    </TouchableOpacity>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    mapStyle: {
      width: Dimensions.get("window").width,
      height: Dimensions.get("window").height,
    },
});