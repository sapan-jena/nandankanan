import React,{Component} from 'react';
import { StyleSheet, View, Dimensions,TouchableOpacity,StatusBar,BackHandler,Linking,Image} from 'react-native';
import {Container,Content,Header,Text,Spinner, Icon, Grid,Left,Body,Right, List,ListItem, Card, CardItem,} from 'native-base';
import Logo from '../../images/logo.webp';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
import {ZooTimeData} from '../config/data';
import NHeader from './components/header'

export default class ZooTime extends Component{
    constructor(props){
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }
    onBackPress = () => {
        this.props.navigation.navigate("Landing");
        return true;
    };
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
    }


    render(){
        return(
            <Container>
                <NHeader onPressHome={()=>this.props.navigation.navigate('Landing')} 
                    name="ZOO TIME"
                />
                
                <StatusBar barStyle="light-content"  backgroundColor="#0EA697" />
                <Content style={{paddingTop:10}}>
                    {/* <Text style={{marginBottom:10,textAlign:'center',color:'#fff',fontSize:18,fontWeight:'bold',backgroundColor:"rgb(40, 153, 87)",padding:10}}>ZOO TIME</Text> */}
                    {
                        ZooTimeData.map((row,index)=>{
                        return <Card key={index} style={{marginLeft:10,marginRight:10}}>
                            <CardItem header style={{backgroundColor:'#ddd'}}>
                        <Text style={{color:'#0EA697',fontWeight:'bold'}}>{row.header}</Text>
                            </CardItem>
                            <CardItem bordered>
                                <Body>   
                                    <Text style={{color:'#000'}}>
                                        <Icon type="AntDesign" name="calendar" style={{color:'#0EA697',marginRight:10,fontSize:16}}/>
                                        <Text style={{fontWeight:'bold'}}>{row.month1}</Text>
                                        <Text> {row.time1}</Text>
                                    </Text>
                                    <Text style={{color:'#000'}}>
                                        <Icon type="AntDesign" name="calendar" style={{color:'#0EA697',marginRight:10,fontSize:16}}/>
                                        <Text style={{fontWeight:'bold'}}>{row.month2}</Text>
                                        <Text> {row.time2}</Text>
                                    </Text> 
                                </Body>
                            </CardItem>
                            <CardItem footer style={{backgroundColor:'#ddd'}}>
                                <Text style={{color:'#000',fontWeight:'bold'}}>
                                    <Text style={{color:'red'}}>Note : </Text>
                                    {row.footer}
                                </Text>
                            </CardItem>
                        </Card>
                   })}
                </Content>
            </Container>
        );
    }
}