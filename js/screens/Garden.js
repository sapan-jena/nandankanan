import React,{Component} from 'react';
import { StyleSheet, View, Dimensions,Linking,TouchableOpacity,StatusBar,BackHandler,Image} from 'react-native';
import {Container,Content,Header,Text,Spinner, Icon, Grid,Left,Body,Right ,Card,CardItem} from 'native-base';
import Logo from '../../images/logo.webp';
import BotanicalGarden from '../../images/botanical.jpg';
import {GardenDetails} from '../config/data';
import NHeader from './components/header'

export default class Garden extends Component{
    constructor(props){
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }
    onBackPress = () => {
        this.props.navigation.navigate("Landing");
        return true;
    };
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
    }


    render(){
        return(
            <Container>
                <NHeader onPressHome={()=>this.props.navigation.navigate('Landing')} 
                    name="BOTANICAL GARDEN"
                />
{/*                 
                <Header hasTabs style={{backgroundColor:'#ffffff',borderBottomColor:"#ddd",borderBottomWidth:2}}>
                    <Left style={{flexDirection:'row'}}>
                        <TouchableOpacity transparent onPress={() => this.props.navigation.navigate('Landing')}>
                            <Icon type="Ionicons" name='md-arrow-back' style={{ fontSize: 25, color: '#0EA697',marginRight:10 }} />
                        </TouchableOpacity>
                        <TouchableOpacity transparent>
                        <Text numberOfLines={1} style={{color:'#0EA697',width:200,fontWeight:'bold',fontSize:20}}>BOTANICAL GARDEN</Text>
                     
                        </TouchableOpacity>
                    </Left>
                    <Right>
                        <TouchableOpacity transparent onPress={() => this.props.navigation.navigate('Landing')}><Icon type="AntDesign" name="home" style={{color:'#0EA697',marginRight:20}}/></TouchableOpacity>
                        <TouchableOpacity transparent onPress={()=>{Linking.openURL('tel:+919437571033');}}><Icon type="Feather" name="phone-call" style={{color:'#0EA697',marginRight:20}}/></TouchableOpacity>
                       
                       </Right>
                </Header> */}
                <StatusBar barStyle="light-content"  backgroundColor="#0EA697" />
                <Content style={{paddingTop:10}}>
                    {/* <Text style={{marginBottom:10,textAlign:'center',color:'#fff',fontSize:18,fontWeight:'bold',backgroundColor:"rgb(40, 153, 87)",padding:10}}>STATE BOTANICAL GARDEN</Text> */}
                    <Card style={{marginLeft:10,marginRight:10}}>
                        <CardItem cardBody>
                            <Image source={BotanicalGarden} style={{height: 200, width: null, flex: 1}}/>
                        </CardItem>
                        <CardItem footer>
                            <Text style={{color:'#0EA697',textAlign:"justify"}}>
                            The State Botanical Garden spreads over an area of 75 ha. It is situated in the sylvan settings of the moist deciduous forest of the sprawling  rgb(40, 153, 87) Nandankanan sanctuary hemmed between two wetlands. The Botanical Garden was established in the year 1963 and was under the management of Horticulture Department. The management of the State Botanical Garden was transferred to Nandankanan on 1st August 2006. One would be definitely impressed by the nature’s symphony and the exquisite touch of the wilderness here. This is one of the pioneering plant conservation and nature education centre of the State. The State Botanical Garden is sure to provide memorable experience to the  rgb(40, 153, 87) enthusiasts. The State Botanical Garden is situated inside Nandankanan wildlife sanctuary. Kiakani Lake with an area about 25 ha. is located inside the State Botanical Garden.
                            </Text>
                        </CardItem>
                    </Card>
                    {GardenDetails.map((row,index)=>{
                        return <Card key={index} style={{marginLeft:10,marginRight:10}}>
                            <CardItem header style={{backgroundColor:'#ddd'}}>
                                <Text style={{color:'#0EA697',fontWeight:'bold'}}>{row.header}</Text>
                            </CardItem>
                            <CardItem footer>
                                <Text note style={{textAlign:"justify"}}>
                                    {row.body}
                                </Text>
                            </CardItem>
                        </Card>
                    })}
                    <Card style={{marginLeft:10,marginRight:10}}>
                        <CardItem header style={{backgroundColor:'#ddd'}}>
                            <Text style={{color:'#0EA697',fontWeight:'bold'}}>Objectives</Text>
                        </CardItem>
                        <CardItem>
                            <View>
                                <Text note style={{textAlign:"justify"}}>
                                    <Icon type="MaterialCommunityIcons" name="star-four-points-outline" style={{color:'#0EA697',fontSize:16}}/>
                                    Conservation, study and propagation of rare plants.
                                </Text>
                                <Text note style={{textAlign:"justify"}}>
                                    <Icon type="MaterialCommunityIcons" name="star-four-points-outline" style={{color:'#0EA697',fontSize:16}}/>
                                    Collection, documentation and maintenance of indigenous and exotic plant species and serve as a conservation and education center.
                                </Text>
                                <Text note style={{textAlign:"justify"}}>
                                    <Icon type="MaterialCommunityIcons" name="star-four-points-outline" style={{color:'#0EA697',fontSize:16}}/>
                                    Acclimatization and field evaluation of indigenous and exotic plants from various agro-climatic regions.
                                </Text>
                                <Text note style={{textAlign:"justify"}}>
                                    <Icon type="MaterialCommunityIcons" name="star-four-points-outline" style={{color:'#0EA697',fontSize:16}}/>
                                    Establishing herbarium of authentic specimen for identification of plants.
                                </Text>
                                <Text note style={{textAlign:"justify"}}>
                                    <Icon type="MaterialCommunityIcons" name="star-four-points-outline" style={{color:'#0EA697',fontSize:16}}/>
                                    Sale and exchange of seeds, plants and other planting materials to individuals and research institutions.
                                </Text>
                                <Text note style={{textAlign:"justify"}}>
                                    <Icon type="MaterialCommunityIcons" name="star-four-points-outline" style={{color:'#0EA697',fontSize:16}}/>
                                    Provide recreation facilities to the public in general and creating awareness on Biodiversity conservation.
                                </Text>
                            </View>
                        </CardItem>
                        <CardItem footer style={{backgroundColor:'#ddd'}}>
                            <Text style={{color:'#0EA697',fontWeight:'bold'}}>
                                <Text style={{color:'red', textAlign:"justify"}}>Note : </Text>
                                State Botanical Garden is coming under Eco-sensitive Zone under Nandankanan Sanctuary. No meeting and conference involving the sound of high decibel shall be allowed inside Botanical Garden.</Text>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}