import React,{Component} from 'react';
import { StyleSheet, View, Dimensions,TouchableOpacity,StatusBar,BackHandler,Image,Share,Linking} from 'react-native';
import {Container,Content,Header,Text,Spinner, Icon, Grid,Left,Body,Footer, FooterTab,Button,Right  } from 'native-base';
import getDirections from 'react-native-google-maps-directions';
import {LearnMoreLinks,Colors,DebugInstructions,ReloadInstructions} from 'react-native/Libraries/NewAppScreen';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
import AnimalImage from '../../images/appIcon.webp';
import Logo from '../../images/logo.webp';

export default class ZooAnimalDetails extends Component{
    constructor(props){
        super(props);
        this.state = {
            data:this.props.navigation.state.params.data,
            active:false,
            backRoute:this.props.navigation.state.params.backRoute,
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }
    onBackPress = () => {
        this.props.navigation.navigate(this.state.backRoute);
        return true;
    };
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
    }
    componentWillMount(){
        console.log(this.state.data)
    }

    shareDetails = () => {
        alert('share ur details')
    };
    getMapDirection=()=>{
        alert('google map direction')
    }
    onShare(){
        try {
            const result = Share.share({
              message:this.state.data.title + this.state.data.url,
            });
          } catch (error) {
            alert(error.message);
        }
    };
    getRoute(){
        const data={
            destination: {
                latitude: parseFloat(this.state.data.latitude),
                longitude: parseFloat(this.state.data.longitude)
            },
        }
        getDirections(data);    
    }

    render(){
        return(
            <Container>
                 <NHeader onPressHome={()=>this.props.navigation.navigate('Landing')} 
                    name="Animal Details"
                />
               
               
                <StatusBar barStyle="light-content"  backgroundColor="rgb(40, 153, 87)" />
                <Content>
        <Text style={{marginBottom:10,textAlign:'center',color:'#fff',fontSize:18,fontWeight:'bold',backgroundColor:"rgb(40, 153, 87)",padding:10}}>{this.state.data.title}</Text>
                    <Image source={AnimalImage}  style={{height:screenHeight/2-100,width:screenWidth}}/>
                        <View style={styles.body}>
                            <View style={styles.sectionContainer}>
                                <Text style={styles.sectionTitle}>Step One</Text>
                                <Text style={styles.sectionDescription}>
                                    Edit <Text style={styles.highlight}>App.js</Text> to change this
                                    screen and then come back to see your edits.
                                </Text>
                            </View>
                            <View style={styles.sectionContainer}>
                                <Text style={styles.sectionTitle}>See Your Changes</Text>
                                <Text style={styles.sectionDescription}>
                                    <ReloadInstructions />
                                </Text>
                            </View>
                            <View style={styles.sectionContainer}>
                                <Text style={styles.sectionTitle}>Learn More</Text>
                                <Text style={styles.sectionDescription}>
                                    Read the docs to discover what to do next: Read the docs to discover what to do next: Read the docs to discover what to do next: Read the docs to discover what to do next:
                                </Text>
                            </View>
                        </View>
                </Content>
                <Footer>
                    <FooterTab style={{backgroundColor:"rgb(40, 153, 87)"}}>
                        <Button style={{backgroundColor:"rgb(40, 153, 87)"}}  onPress={() => this.onShare()}>
                            <Icon style={{fontSize:14,color:'#fff'}} type="FontAwesome5" name="share-alt"/>
                            <Text style={{fontSize:14,color:'#fff'}}>Share</Text>
                        </Button>
                        <Button style={{backgroundColor:'#045e04'}} onPress={()=>this.getRoute()}>
                            <Icon style={{fontSize:14,color:'#fff'}}  type="FontAwesome5" name="map-marked-alt"/>
                            <Text style={{fontSize:14,color:'#fff'}}>Map</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    scrollView: {
      backgroundColor: Colors.lighter,
    },
    engine: {
      position: 'absolute',
      right: 0,
    },
    body: {
      backgroundColor: Colors.white,
    },
    sectionContainer: {
      marginTop: 32,
      paddingHorizontal: 24,
    },
    sectionTitle: {
      fontSize: 24,
      fontWeight: '600',
      color: Colors.black,
    },
    sectionDescription: {
      marginTop: 8,
      fontSize: 18,
      fontWeight: '400',
      color: Colors.dark,
    },
    highlight: {
      fontWeight: '700',
    },
});