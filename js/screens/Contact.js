import React,{Component} from 'react';
import { StyleSheet, View, Dimensions,TouchableOpacity,StatusBar,BackHandler,Image,Linking} from 'react-native';
import {Container,Content,Header,Text,Spinner, Icon, Grid,Left,Body,Right, List,ListItem,Thumbnail, Card, CardItem, Separator,} from 'native-base';
const screenWidth = Dimensions.get("window").width;
import Logo from '../../images/logo.webp';
import {getContactDetails} from '../config/api'
import NHeader from './components/header'

export default class Contact extends Component{
    constructor(props){
        super(props);
        this.state = {
            data:[],
            loading:true,
            error:false
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        getContactDetails((successResponse)=>{
            this.setState({data:successResponse,loading:false,error:false})
            // console.log(this.state.data)
        },(error)=>{
            // alert(error);
            this.setState({loading:false,error:true})
        })
    }
    onBackPress = () => {
        this.props.navigation.navigate("Landing");
        return true;
    };
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
    }


    render(){
        return(
            <Container>
                 
                <NHeader onPressHome={()=>this.props.navigation.navigate('Landing')} 
                    name="CONTACTS"
                />
                
                <StatusBar barStyle="light-content"  backgroundColor="#0EA697" />
                <Content style={{paddingTop:15,paddingBottom:15}}>
                    {/* <Text style={{marginBottom:10,textAlign:'center',color:'#fff',fontSize:18,fontWeight:'bold',backgroundColor:"rgb(40, 153, 87)",padding:10}}>CONTACTS</Text> */}
                   
                    {this.state.loading?<View><Spinner style={{marginTop:120}} />
                    <Text style={{textAlign:'center',fontSize:20 }} note>Please wait..</Text>
                    </View>:<View />}
                    {this.state.error?<View style={{padding:20,alignSelf:'center',alignItems:'center',justifyContent:'center',marginTop:120}}>
                    <Icon style={{color:'grey',fontSize:50,fontWeight:'bold',marginBottom:10}} name="unlink" type="FontAwesome" />
                        <Text note style={{fontSize:22}}>Your Internet may be disconnected. Go back to </Text>
                        <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}><Text style={{fontSize:20,fontWeight:'bold'}}>Home</Text></TouchableOpacity>
                    </View>:<View/>}
                   
                    {this.state.data.map((data,i) => {
                        return (
                        <View key={i} style={{flexDirection:'row',marginBottom:15,marginTop:3}}>
                            <View style={{marginLeft:15,marginRight:15}}>
                                <Thumbnail source={{uri:data.url}} square large style={{borderColor:'#ddd',borderWidth:2}} />
                            </View>
                            <View style={{flexDirection:'column',alignItems:'flex-start',marginTop:-6}}>
                                <Text style={{color:'#27AA5D',fontWeight:'bold',width:screenWidth-130}}>{data.designation}</Text>
                                <View style={{borderBottomColor: 'black', borderBottomWidth: 1,}}/>
                                {data.name!=="" ? <Text style={{color:'#1677D9',fontWeight:'bold'}}>{data.name}</Text>:<React.Fragment/>}
                                {/* <Text note>{data.title}</Text> */}
                                {data.Address!=="" ? <Text note style={{width:screenWidth-130,color:"#000"}}>{data.Address}</Text>:<React.Fragment/>}
                                <Text note style={{color:"#000"}}>Phone: {data.Phone}</Text>
                                {data.Fax !== ""? <Text note style={{color:"#000"}}>Fax: {data.Fax}</Text>:<React.Fragment/>}
                                {data.email!=="" ? <Text note style={{width:screenWidth-130,color:"#000"}}>E-mail:  {data.email}</Text>:<React.Fragment/>}
                            </View>
                        </View>
                    )})}
                </Content>
            </Container>
        );
    }
}