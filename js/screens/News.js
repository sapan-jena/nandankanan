import React,{Component} from 'react';
import { StyleSheet, View, Dimensions,TouchableOpacity,StatusBar,BackHandler,Image,Linking} from 'react-native';
import {Container,Content,Header,Text,Spinner, Icon, Grid,Left,Body,Right ,List,ListItem,Button,Card, CardItem,} from 'native-base';
import Logo from '../../images/logo.webp';
import {getNewsDetails} from '../config/api';
import NewsImage from '../../images/new.gif'
import NHeader from './components/header'


const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class News extends Component{
    constructor(props){
        super(props);
        this.state = {
            data:[],
            loading:true,
            error:false
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        getNewsDetails((successResponse)=>{
            this.setState({data:successResponse,loading:false,error:false})
        },(error)=>{
            this.setState({error:true,loading:false})
            // alert(error);
        })
    }
    onBackPress = () => {
        this.props.navigation.navigate("Landing");
        return true;
    };
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
    }


    render(){
        return(
            <Container>

                <NHeader onPressHome={()=>this.props.navigation.navigate('Landing')} 
                                    name="NEWS"
                                />
                
                
                <StatusBar barStyle="light-content"  backgroundColor="#0EA697" />
                <Content style={{paddingTop:10}}>
                    {/* <Text style={{marginBottom:10,textAlign:'center',color:'#fff',fontSize:18,fontWeight:'bold',backgroundColor:"rgb(40, 153, 87)",padding:10}}>NEWS</Text> */}
                    
                    {this.state.loading?<View><Spinner style={{marginTop:120}} />
                    <Text style={{textAlign:'center',fontSize:20 }} note>Please wait..</Text>
                    </View>:<View />}
                    {this.state.error?<View style={{padding:20,alignSelf:'center',alignItems:'center',justifyContent:'center',marginTop:120}}>
                    <Icon style={{color:'grey',fontSize:50,fontWeight:'bold',marginBottom:10}} name="unlink" type="FontAwesome" />
                        <Text note style={{fontSize:22}}>Your Internet may be disconnected. Go back to </Text>
                        <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}><Text style={{fontSize:20,fontWeight:'bold'}}>Home</Text></TouchableOpacity>
                    </View>:<View/>
                    }


                    <List>
                    {
                        this.state.data.map((data,i) => {
                            return (
                                <Card key={i} style={{marginLeft:10,marginRight:10,borderLeftWidth:5,borderLeftColor:'green'}}>
                                    <ListItem itemDivider >
                                        <Text style={{color:'#0EA697',textAlign:'justify'}}>
                                            {/* <Icon type="MaterialCommunityIcons" name="star-four-points-outline" style={{color:'#000',marginRight:10,fontSize:16}}/> */}
                                            {data.title}
                                            {data.new==1?
                                                <Image source={NewsImage} alt="new news"/>
                                            :null}
                                        </Text>
                                        
                                    </ListItem>                    
                                    <ListItem>
                                        <Left>
                                            <Icon type="AntDesign" name="calendar" style={{color:'#0EA697',marginRight:10}}/>
                                            <Text style={{color:'black'}} note>Publish Date: {data.publish_date}</Text>
                                        </Left>
                                        <Right>
                                            <TouchableOpacity transparent onPress={ ()=>{ Linking.openURL(data.url)}}>
                                                <Icon style={{color:'#0EA697'}} name="arrow-forward" />
                                            </TouchableOpacity>
                                        </Right>
                                    </ListItem>
                                </Card>
                        )})}
                    </List>
                </Content>
            </Container>
        );
    }
}