//import liraries
import React, { Component } from 'react';
import { View,Image,StatusBar,Dimensions } from 'react-native';
import { Container, Content, Spinner} from 'native-base';
import splashlogo from '../../images/appIcon.webp';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

// create a component
export default class Splash extends Component {
    componentDidMount(){
        setTimeout(()=>this.props.navigation.navigate("Landing"))
    }
    
    render() {
        return (
            <Container>
                <StatusBar barStyle="light-content"  backgroundColor="#0EA697" />
                <Content style={{backgroundColor:"#0EA697",height:screenHeight,}}>
                    <View style={{alignItems: 'center', justifyContent: 'center',height:screenHeight,}}>
                        <Image source={splashlogo} resizeMode="contain" style={{ width: 150}}></Image> 
                    </View>
                    <Spinner color='#fff' />
                </Content>
            </Container>

        );
    }
}

