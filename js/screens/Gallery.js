import React,{Component} from 'react';
import { ScrollView, View, Dimensions,TouchableOpacity,StatusBar,BackHandler,Image,Linking,FlatList} from 'react-native';
import {Container,Content,Header,Text,Spinner, Icon, Grid,Left,Body,Right, List,ListItem, Card, CardItem,} from 'native-base';
import Logo from '../../images/logo.webp';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
import {getPhotoGalleryDetails} from '../config/api';
import NHeader from './components/header'
import ImageView from 'react-native-image-view';


export default class Gallery extends Component{
    constructor(props){
        super(props);
        this.state = {
            data:[],
            loading:true,
            error:false,
            isImageViewVisible: false,
            imageIndex: 0,
            current_index:0,
            caption:'',
        };
        this.renderFooter = this.renderFooter.bind(this);
    }

    renderFooter({title}) {
        const {likes} = this.state;

        return (
            <View>
                <Text>{title}</Text>
            </View>
        );
    }

    componentDidMount() {
        this.setState({loading:true})
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        getPhotoGalleryDetails((successResponse)=>{
            this.setState({data:successResponse,loading:false,error:false})
        },(error)=>{
            this.setState({loading:false,error:true})
            // alert(error);
        })
    }
    onBackPress = () => {
        this.props.navigation.navigate("Landing");
        return true;
    };
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
    }

    render(){
        const {isImageViewVisible, imageIndex} = this.state;
        let current_index = this.state.current_index
        return(
            <Container>
                <NHeader onPressHome={()=>this.props.navigation.navigate('Landing')} 
                    name="PHOTO GALLERY"
                />
                
                <StatusBar barStyle="light-content"  backgroundColor="#0EA697" />
                <Content>
                {/* <Text style={{marginBottom:10,textAlign:'center',color:'#fff',fontSize:18,fontWeight:'bold',backgroundColor:"rgb(40, 153, 87)",padding:10}}>PHOTO GALLERY</Text> */}
                   
                {this.state.loading?<View><Spinner style={{marginTop:120}} />
                    <Text style={{textAlign:'center',fontSize:20 }} note>Please wait..</Text>
                    </View>:<View />}
                    {this.state.error?<View style={{padding:20,alignSelf:'center',alignItems:'center',justifyContent:'center',marginTop:120}}>
                    <Icon style={{color:'grey',fontSize:50,fontWeight:'bold',marginBottom:10}} name="unlink" type="FontAwesome" />
                        <Text note style={{fontSize:22}}>Your Internet may be disconnected. Go back to </Text>
                        <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}><Text style={{fontSize:20,fontWeight:'bold'}}>Home</Text></TouchableOpacity>
                    </View>:<View/>}
                     
                    <ScrollView>
                        <View style={{flex:1,justifyContent:'center'}}>
                            <FlatList
                                data={this.state.data}
                                renderItem={({ item, index }) => (
                                    <View style={{ flex: 1, flexDirection: 'column',padding:8,margin:1, }}>
                                        <TouchableOpacity 
                                            onPress={() => {
                                                this.setState({
                                                    isImageViewVisible: true,
                                                    current_index:index
                                                });
                                            }}>
                                            <Image style={{height: 110, width: null,}} source={{ uri: item.url }} />
                                        </TouchableOpacity>
                                    </View>
                                )}
                                //Setting the number of column
                                numColumns={2}
                                keyExtractor={(item, index) => index.toString()}
                            />

                                <ImageView 
                                    glideAlways
                                    images={this.state.data.map((item) =>{
                                        return {
                                            source: { 
                                                uri: item.url,
                                            },
                                            title: item.caption,
                                            width: screenWidth,
                                            height: 250,
                                        };
                                    })}
                                    imageIndex={current_index}
                                    isVisible={isImageViewVisible}
                                    onClose={() => this.setState({isImageViewVisible: false})}
                                    onImageChange={i => {
                                        current_index=i
                                    }}
                                    renderFooter={(currentImage) => {
                                        return (<View><Text style={{color:'#fff',textAlign:'center',fontWeight:'bold',marginBottom:30}}>{currentImage.title}</Text></View>)}}
                                />
                          
                        </View>
                    </ScrollView>
                </Content>
            </Container>
        );
    }
}
