import React,{Component} from 'react';
import { StyleSheet, View, Dimensions,TouchableOpacity,StatusBar,Linking,BackHandler,Image} from 'react-native';
import {Container,Content,Header,Text,Spinner, Icon, Grid,Left,Body,Right } from 'native-base';
import MapView, {Marker ,Callout} from 'react-native-maps';
import Logo from '../../images/logo.webp';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
import {ZoologicalMapDetails} from '../config/data';
import getDirections from 'react-native-google-maps-directions';
// import Geolocation from '@react-native-community/geolocation';
import NHeader from './components/header'
const earthRadiusInKM = 6371;
// you can customize these two values based on your needs
const radiusInKM = 0.1;
const aspectRatio = 1;

export default class ZoologicalPark extends Component{
    constructor(props){
        super(props);
        this.state = {
            latitude: 20.395775,
            longitude: 85.8237703,
            my_latitude: 0,
            my_longitude: 0,
            region:{
                latitude: 20.395775,
                longitude: 85.8237703,
                latitudeDelta:0.0008993216059187306,
                longitudeDelta:0.0009594731963041548
            }
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        // Geolocation.getCurrentPosition((info) => {
        //     console.log(info);
        //     // this.setState({
        //     //     my_latitude: info.coords.latitude,
        //     //     my_longitude: info.coords.longitude,
        //     //     error: null,
        //     // });
        // })
        this.showRegion({latitude: 20.395775,
            longitude: 85.8237703})
    }

    deg2rad (angle) {
        return angle * 0.017453292519943295 // (angle / 180) * Math.PI;
    }

    rad2deg (angle) {
        return angle * 57.29577951308232 // angle / Math.PI * 180
    }

    // you need to invoke this method to update your map's region. 
    showRegion(locationCoords) {
        if (locationCoords && locationCoords.latitude && locationCoords.longitude) {
            let radiusInRad = radiusInKM / earthRadiusInKM;
            let longitudeDelta = this.rad2deg(radiusInRad / Math.cos(this.deg2rad(locationCoords.latitude)));
            let latitudeDelta = aspectRatio * this.rad2deg(radiusInRad);
            console.log(latitudeDelta)
            console.log(longitudeDelta)
            this.setState({
                region: { latitude: locationCoords.latitude, longitude: locationCoords.longitude, latitudeDelta: latitudeDelta, longitudeDelta: longitudeDelta }
            });
        }
    }


    onBackPress = () => {
        this.props.navigation.navigate("ZooMap");
        return true;
    };
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
    }

    // SetClass(data){
    //     data=data.data;
    //     console.log('data');
    //     console.log(data);
    //     if(data.length > 0){
    //       this.props.navigation.navigate('ZooAnimalDetails',{data:data});
    //     }else{
    //       alert("Data not found");
    //       this.props.navigation.navigate('Landing');
    //     }
    // }


    render(){
        return(
            <Container>
                 <NHeader onPressHome={()=>this.props.navigation.navigate('Landing')} 
                    name="Zoological Park"
                />
                
                
                <StatusBar barStyle="light-content"  backgroundColor="#0EA697" />
                <Content>
                    {/* <Text style={{textAlign:'center',color:'#fff',fontSize:18,fontWeight:'bold',backgroundColor:"rgb(40, 153, 87)",padding:10}}>Zoological Park</Text> */}
                    <MapView style={styles.mapStyle} showsMyLocationButton={true} 
                        minZoomLevel={16} 
                        maxZoomLevel={25}
                        showsUserLocation={true}
                        setMapBoundaries={
                            { latitude: 20.395320, longitude: 85.797662 },
                            { latitude: 20.382226, longitude: 85.821272 },
                            { latitude: 20.404184, longitude: 85.837234 },
                            { latitude: 20.416129, longitude: 85.821030 }
                        }
                        initialRegion={{
                            latitude:this.state.latitude,
                            longitude:this.state.longitude,
                            latitudeDelta: 0.0522,
                            longitudeDelta: 0.0821
                        }}>
                        {ZoologicalMapDetails.map((data,i) => {
                         
                            return (
                            <Marker
                                coordinate={{
                                    latitude: data.latitude,
                                    longitude: data.longitude
                                }}
                                title={data.title}
                                description={"Navigate →"}
                                key={i}
                                image={data.url}
                                onCalloutPress={()=>{
                                    const data_={
                                        destination: {
                                            latitude: parseFloat(data.latitude),
                                            longitude: parseFloat(data.longitude)
                                        },
                                    }
                                    getDirections(data_)
                                }}
                            >
                            </Marker>  
                        )})}
                    </MapView>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    mapStyle: {
      width: Dimensions.get("window").width,
      height: Dimensions.get("window").height,
    },
});