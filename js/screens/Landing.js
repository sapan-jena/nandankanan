import React,{Component} from 'react';
import {StyleSheet,TouchableOpacity,Image,StatusBar,View,Dimensions,BackHandler,ImageBackground,Linking,ScrollView } from 'react-native';
import {Container,Content,Header,Text,Spinner, Icon, Grid, Left,Right,Body} from 'native-base';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
import ImageSlider from 'react-native-image-slider';
import Backgroundimage from '../../images/body-bg.png';
import Logo from '../../images/logo.webp';
import Ticket from '../../images/tickets.png';
import Adoption from '../../images/adopt.png';
import Zoo_map from '../../images/zoo-map.png';
import Zoo_time from '../../images/zoo-time.png';
import Amenities from '../../images/amenities.png';
import Garden from '../../images/garden.png';
import Gallery from '../../images/gallery.png';
import Grievance from '../../images/grievance.webp';
import Tender from '../../images/feedback.png';
import News from '../../images/news.png';
import Contact from '../../images/contact.png';
import Quiz from '../../images/inventory.png';


export default class Landing extends Component{
    constructor(props){
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }
    onBackPress = () => {
        BackHandler.exitApp()
    };
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
    }

    render(){
        const images=[
            require('../../images/banner/banner_1513173089.jpg'),
            require('../../images/banner/banner_1513172978.jpg'),
            require('../../images/banner/banner_1513173042.jpg'),
            require('../../images/banner/banner_1513173129.jpg'),
            require('../../images/banner/banner_1513173223.jpg'),
            require('../../images/banner/banner_1513173267.jpg'),
            require('../../images/banner/banner_1513173327.jpg'),
            require('../../images/banner/banner_1513173363.jpg'),
            require('../../images/banner/banner_1562308966.jpg'),
            require('../../images/banner/banner_1562309093.jpg'),
            require('../../images/banner/banner_1562309494.jpg'),
            require('../../images/banner/banner_1562309524.jpg'),
            require('../../images/banner/banner_1562309537.jpg'),
        ]
        return(
            <Container>
                <Header hasTabs style={{backgroundColor:'#ffffff',}}>
                    <Left>
                        <TouchableOpacity transparent>
                           <Image source={Logo} style={{width:120,height:37}} />
                        </TouchableOpacity>
                    </Left>
                    <Right>
                        <TouchableOpacity transparent onPress={() => this.props.navigation.navigate('Landing')}><Icon   type="AntDesign" name="home" style={{color:'#0EA697',marginRight:20}}/></TouchableOpacity>
                        <TouchableOpacity transparent onPress={()=>{Linking.openURL('tel:+919438918584');}}><Icon type="Feather" name="phone-call" style={{color:'#0EA697',marginRight:20}}/></TouchableOpacity>
                    </Right>
                </Header>
                <StatusBar barStyle="light-content"  backgroundColor="rgb(40, 153, 87)" />
                <ScrollView>
                    <ImageSlider  images={images} loopBothSides autoPlayWithInterval={3000} style={{height:screenHeight/3.5}} customButtons={() => (null)}/>
                    <ImageBackground source={Backgroundimage} style={{width:screenWidth,backgroundColor:"rgb(40, 153, 87)",paddingBottom:10}}>
                        <View style={{borderWidth:2,borderColor:'#307249'}}></View>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("Webview",{webview:'https://www.nandankanan.org/mobile/tickets/'})}
                                style={styles.touchableButton}>
                                <Image source={Ticket} style={styles.appicon}/>
                                <Text style={styles.apptext}>Tickets</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("Webview",{webview:'https://www.nandankanan.org/mobile/adoption.php'})}
                                style={styles.touchableButton}>
                                <Image source={Adoption} style={styles.appicon}/>
                                <Text style={styles.apptext}>Adoption</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("ZooMap")}
                                style={styles.touchableButton}>
                                <Image source={Zoo_map} style={styles.appicon}/>
                                <Text style={styles.apptext}>Zoo-map</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("ZooTime")}
                                style={styles.touchableButton}>
                                <Image source={Zoo_time} style={styles.appicon}/>
                                <Text style={styles.apptext}>Zoo-time</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("Amenities")}
                                style={styles.touchableButton}>
                                <Image source={Amenities} style={styles.appicon}/>
                                <Text style={styles.apptext}>Amenities</Text>
                            </TouchableOpacity>
                            <TouchableOpacity  onPress={() => this.props.navigation.navigate("Garden")}
                                style={styles.touchableButton}>
                                <Image source={Garden} style={styles.appicon}/>
                                <Text style={styles.apptext}>Garden</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <TouchableOpacity  onPress={() => this.props.navigation.navigate("Gallery")}
                                style={styles.touchableButton}>
                                <Image source={Gallery} style={styles.appicon}/>
                                <Text style={styles.apptext}>Gallery</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                            onPress={() => this.props.navigation.navigate("Grievance")}
                                style={styles.touchableButton}>
                                <Image source={Grievance} style={styles.appicon}/>
                                <Text style={styles.apptext}>Grievance</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("Webview",{webview:'https://www.nandankanan.org/mobile/feedback.php'})}
                                style={styles.touchableButton}>
                                <Image source={Tender} style={styles.appicon}/>
                                <Text style={styles.apptext}>Feedback</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("News")}
                                style={styles.touchableButton}>
                                <Image source={News} style={styles.appicon}/>
                                <Text style={styles.apptext}>News</Text>
                            </TouchableOpacity>
                            <TouchableOpacity  onPress={() => this.props.navigation.navigate("Contact")}
                                style={styles.touchableButton}>
                                <Image source={Contact} style={styles.appicon}/>
                                <Text style={styles.apptext}>Contact</Text>
                            </TouchableOpacity>
                            <TouchableOpacity 
                            onPress={() => this.props.navigation.navigate("Webview",{webview:'https://www.nandankanan.org/mobile/quiz'})}
                                style={styles.touchableButton}>
                                <Image source={Quiz} style={styles.appicon}/>
                                <Text style={styles.apptext}>Quiz</Text>
                            </TouchableOpacity>
                        </View>
                        </ImageBackground>
                    </ScrollView>
            </Container>
        );
    }

}

const styles = StyleSheet.create({
    touchableButton:{
        width: (Dimensions.get("window").width / 3) - 20, 
        alignItems: "center", 
        justifyContent: "center", 
        elevation: 8, 
        margin: 10,
        marginTop:15,
    },
    appicon:{
        zIndex:1,
        width:77,
        height:77,
    },
    apptext:{
        textAlign:'center',
        fontSize:14,
        width:90,
        color: " rgb(40, 153, 87)", 
        fontWeight: "bold",
        backgroundColor:'#fff',
        padding:6,
        borderRadius: 22,
        position: 'relative',
        marginTop:-10
    },
})
