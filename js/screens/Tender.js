import React,{Component} from 'react';
import { TouchableOpacity,StatusBar,BackHandler,Dimensions,Linking,View} from 'react-native';
import {Container,Content,Header,Text, Icon,Left,Right ,List,ListItem,Card,Spinner} from 'native-base';
import Logo from '../../images/logo.webp';
import NHeader from './components/header'
import {getTenderDetails} from '../config/api';
const screenWidth = Dimensions.get("window").width;


export default class Tender extends Component{
    constructor(props){
        super(props);
        this.state = {
            data:[],
            error:false,
            loading:true
        };
    }

    componentDidMount() {
        this.setState({loading:true})
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        getTenderDetails((successResponse)=>{
            this.setState({data:successResponse})
            // console.log(this.state.data)
            this.setState({loading:false})
        },(error)=>{
            this.setState({error:true,loading:false})
            // alert(error);
        })
    }
    onBackPress = () => {
        this.props.navigation.navigate("Landing");
        return true;
    };
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
    }

    render(){
        return(
            <Container>
                 <NHeader onPressHome={()=>this.props.navigation.navigate('Landing')} 
                    name="TENDERS"
                />
               
                <StatusBar barStyle="light-content"  backgroundColor="#0EA697" />
                <Content>
                    {/* <Text style={{marginBottom:10,textAlign:'center',color:'#fff',fontSize:18,fontWeight:'bold',backgroundColor:"rgb(40, 153, 87)",padding:10}}>TENDERS</Text> */}
                    {this.state.loading?<View><Spinner style={{marginTop:120}} />
                    <Text style={{textAlign:'center',fontSize:20 }} note>Please wait..</Text>
                    </View>:<View />}
                    {this.state.error?<View style={{padding:20,alignSelf:'center',alignItems:'center',justifyContent:'center',marginTop:120}}>
                    <Icon style={{color:'grey',fontSize:50,fontWeight:'bold',marginBottom:10}} name="unlink" type="FontAwesome" />
                        <Text note style={{fontSize:22}}>Your Internet may be disconnected. Go back to </Text>
                        <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}><Text style={{fontSize:20,fontWeight:'bold'}}>Home</Text></TouchableOpacity>
                    </View>:<View/>}
                    <List>
                    {this.state.data.map((data,i) => {
                        return (
                            <Card key={i} style={{marginLeft:10,marginRight:10,borderLeftWidth:5,borderLeftColor:'green'}}>
                                <ListItem itemDivider style={{textAlign:"justify"}}>
                                    <TouchableOpacity transparent onPress={ ()=>{ Linking.openURL(data.url)}}>
                                        <View style={{flexDirection:'row',width:screenWidth-70}}>
                                            <Text style={{color:'#0EA697'}}>
                                                {/* <Icon type="MaterialCommunityIcons" name="star-four-points-outline" style={{color:'#000',marginRight:10,fontSize:16}}/> */}
                                                {data.title}
                                            </Text>
                                            <Icon style={{color:'#0EA697'}} name="download" />
                                        </View>
                                    </TouchableOpacity>
                                </ListItem>                    
                                <ListItem>
                                    <Left>
                                        <Icon type="AntDesign" name="calendar" style={{color:'#0EA697',marginRight:10}}/>
                                        <Text note style={{color:"black"}}>Publish Date: {data.publish_date}</Text>
                                    </Left>
                                </ListItem>
                                <ListItem>
                                    <Left>
                                        <Icon type="AntDesign" name="calendar" style={{color:'red',marginRight:10}}/>
                                        <Text note style={{color:"black"}}>Expire Date: {data.expiry_date}</Text>
                                    </Left>
                                </ListItem>
                            </Card>
                        )})}
                    </List>
                </Content>
            </Container>
        );
    }
}