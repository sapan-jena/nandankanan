import React,{Component} from 'react';
import {Image,TouchableOpacity,StatusBar,BackHandler,Linking} from 'react-native';
import {Container,Content,Header,Text, Icon,Left,Right, Card, CardItem, } from 'native-base';
import Logo from '../../images/logo.webp';
import DrinkingWater from '../../images/Drinking-Water.jpg';
import Toilet from '../../images/Free-Toilet.jpg';
import Visitor from '../../images/holiday-cottage.jpg';
import Restaurants from '../../images/OTDC-Restaurant.jpg';
import Cloak from '../../images/Free-Cloak-Room.jpg';
import Wheel from '../../images/wheel_chair.jpg';
import Emission from '../../images/amenities-BOV.jpg';
import Guide from '../../images/nw-map.jpg';
import Library from '../../images/nw-Library.jpg';
import Baby from '../../images/nw-Baby-Care-House.jpg';
import Parking from '../../images/nw-under-construction.jpg';
import RainShelters from '../../images/nw-rain-shelters.jpg';
import Souvenir from '../../images/nw-sovenirs.jpg';
import FirstAid from '../../images/nw-no-image.jpg';

import NHeader from './components/header'

export default class Amenities extends Component{
    constructor(props){
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }
    onBackPress = () => {
        this.props.navigation.navigate("Landing");
        return true;
    };
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
    }


    render(){
        return(
            <Container>
                <NHeader onPressHome={()=>this.props.navigation.navigate('Landing')} 
                    name="AMENITIES"
                />

            <StatusBar barStyle="light-content"  backgroundColor="#0EA697" />
            <Content style={{paddingTop:10}}>
                {/* <Text style={{marginBottom:10,textAlign:'center',color:'#fff',fontSize:18,fontWeight:'bold',backgroundColor:"rgb(40, 153, 87)",padding:10}}>AMENITIES</Text> */}
                <Card style={{marginLeft:10,marginRight:10}}>
                    <CardItem header style={{backgroundColor:'#ddd'}}>
                        <Text style={{color:'#0EA697',fontWeight:'bold'}}>
                            Drinking Water Kiosks
                        </Text>
                    </CardItem>
                    <CardItem cardBody>
                        <Image source={DrinkingWater} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                </Card>
                <Card style={{marginLeft:10,marginRight:10}}>
                    <CardItem header style={{backgroundColor:'#ddd'}}>
                        <Text style={{color:'#0EA697',fontWeight:'bold'}}>
                            Toilet Complexes
                        </Text>
                    </CardItem>
                    <CardItem cardBody>
                        <Image source={Toilet} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                </Card>
                <Card style={{marginLeft:10,marginRight:10}}>
                    <CardItem header style={{backgroundColor:'#ddd'}}>
                        <Text style={{color:'#0EA697',fontWeight:'bold'}}>
                            Visitor Cottages
                        </Text>
                    </CardItem>
                    <CardItem cardBody>
                        <Image source={Visitor} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                </Card>
                <Card style={{marginLeft:10,marginRight:10}}>
                    <CardItem header style={{backgroundColor:'#ddd'}}>
                        <Text style={{color:'#0EA697',fontWeight:'bold'}}>
                            Restaurants
                        </Text>
                    </CardItem>
                    <CardItem cardBody>
                        <Image source={Restaurants} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                </Card>
                <Card style={{marginLeft:10,marginRight:10}}>
                    <CardItem header style={{backgroundColor:'#ddd'}}>
                        <Text style={{color:'#0EA697',fontWeight:'bold'}}>
                            Cloak Rooms
                        </Text>
                    </CardItem>
                    <CardItem cardBody>
                        <Image source={Cloak} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                </Card>
                <Card style={{marginLeft:10,marginRight:10}}>
                    <CardItem header style={{backgroundColor:'#ddd'}}>
                        <Text style={{color:'#0EA697',fontWeight:'bold'}}>
                            Wheel Chairs
                        </Text>
                    </CardItem>
                    <CardItem cardBody>
                        <Image source={Wheel} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                </Card>
                <Card style={{marginLeft:10,marginRight:10}}>
                    <CardItem header style={{backgroundColor:'#ddd'}}>
                        <Text style={{color:'#0EA697',fontWeight:'bold'}}>
                            Emission Free Battery Operated Vehicles
                        </Text>
                    </CardItem>
                    <CardItem cardBody>
                        <Image source={Emission} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                </Card>
                <Card style={{marginLeft:10,marginRight:10}}>
                    <CardItem header style={{backgroundColor:'#ddd'}}>
                        <Text style={{color:'#0EA697',fontWeight:'bold'}}>
                            Guide Maps
                        </Text>
                    </CardItem>
                    <CardItem cardBody>
                        <Image source={Guide} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                </Card>
                <Card style={{marginLeft:10,marginRight:10}}>
                    <CardItem header style={{backgroundColor:'#ddd'}}>
                        <Text style={{color:'#0EA697',fontWeight:'bold'}}>
                            Library
                        </Text>
                    </CardItem>
                    <CardItem cardBody>
                        <Image source={Library} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                </Card>
                <Card style={{marginLeft:10,marginRight:10}}>
                    <CardItem header style={{backgroundColor:'#ddd'}}>
                        <Text style={{color:'#0EA697',fontWeight:'bold'}}>
                            Baby Care House
                        </Text>
                    </CardItem>
                    <CardItem cardBody>
                        <Image source={Baby} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                </Card>
                <Card style={{marginLeft:10,marginRight:10}}>
                    <CardItem header style={{backgroundColor:'#ddd'}}>
                        <Text style={{color:'#0EA697',fontWeight:'bold'}}>
                            R.O Water Plant
                        </Text>
                    </CardItem>
                    <CardItem cardBody>
                        <Image source={DrinkingWater} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                </Card>
                <Card style={{marginLeft:10,marginRight:10}}>
                    <CardItem header style={{backgroundColor:'#ddd'}}>
                        <Text style={{color:'#0EA697',fontWeight:'bold'}}>
                            Parking Areas
                        </Text>
                    </CardItem>
                    <CardItem cardBody>
                        <Image source={Parking} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                </Card>
                <Card style={{marginLeft:10,marginRight:10}}>
                    <CardItem header style={{backgroundColor:'#ddd'}}>
                        <Text style={{color:'#0EA697',fontWeight:'bold'}}>
                            Sit Outs and Rain Shelters
                        </Text>
                    </CardItem>
                    <CardItem cardBody>
                        <Image source={RainShelters} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                </Card>
                <Card style={{marginLeft:10,marginRight:10}}>
                    <CardItem header style={{backgroundColor:'#ddd'}}>
                        <Text style={{color:'#0EA697',fontWeight:'bold'}}>
                            Souvenir Shop
                        </Text>
                    </CardItem>
                    <CardItem cardBody>
                        <Image source={Souvenir} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                </Card>
                <Card style={{marginLeft:10,marginRight:10}}>
                    <CardItem header style={{backgroundColor:'#ddd'}}>
                        <Text style={{color:'#0EA697',fontWeight:'bold'}}>
                            First Aid Facilities
                        </Text>
                    </CardItem>
                    <CardItem cardBody>
                        <Image source={FirstAid} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                </Card>
            </Content>
        </Container>
        );
    }
}