import React, {Component} from 'react';
import {
  BackHandler,
  Text,
  Dimensions,
  View,
  Image,
  TouchableOpacity,StatusBar
  
} from 'react-native';
import {WebView} from 'react-native-webview';
import {Icon} from 'native-base'
import splashlogo from '../../images/appIcon.webp';


const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
export default class Webview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      webview: this.props.navigation.state.params.webview,
      opacity: 1,
      web_opacity: 1,
      internet_disconnected: false,
    };
  }

  componentDidMount() {
    this.setState({opacity: 1, web_opacity: 0});
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  onBackPress = () => {
    this.props.navigation.navigate('Landing');
    return true;
  };
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }

  onloadStarted = () => {
    this.setState({opacity: 1, web_opacity: 0});
  };
  onloaded = () => {
    this.setState({opacity: 0, web_opacity: 1});
  };

  render() {
    if(!this.state.internet_disconnected){
    return (
        <WebView
          style={{opacity: this.state.web_opacity}}
          startInLoadingState={true}
          renderLoading={() =>
                <View style={{backgroundColor:"#0EA697",height:screenHeight,justifyContent:"center",alignItems: 'center'}}>
                    <StatusBar barStyle="light-content"  backgroundColor="#0EA697" />
                    <Image source={splashlogo} resizeMode="contain" style={{ width: 150}}></Image> 
                </View>
            }
          onLoadStart={this.onloadStarted}
          onLoad={this.onloaded}
          onError={() => {
            this.setState({internet_disconnected: true});
          }}
          source={{uri: this.state.webview}}
        />
        
    );
  }else{
    return(
      <View style={{padding:20,alignSelf:'center',alignItems:'center',justifyContent:'center',marginTop:120}}>
      <Icon style={{color:'grey',fontSize:50,fontWeight:'bold',marginBottom:10}} name="unlink" type="FontAwesome" />
          <Text note style={{fontSize:22,textAlign:'center'}}>Your Internet may be disconnected. Go back to </Text>
          <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}><Text style={{fontSize:20,fontWeight:'bold'}}>Home</Text></TouchableOpacity>
      </View>
    )
  }
  }
}
