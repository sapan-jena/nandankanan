import React from 'react'
import Backgroundimage from '../../images/forest.png';
import BodyBackgroundimage from '../../images/body-bg.png';
import { StyleSheet, View, Dimensions, TouchableOpacity, StatusBar, BackHandler, ImageBackground, Image, Linking } from 'react-native';
import { Container, Content, Header, Text, Spinner, Icon, Grid, Left, Right, Body } from 'native-base';
import NHeader from './components/header'
import Logo from '../../images/logo.webp';
import MessageImg from '../../images/message.png'
import ProfileImg from '../../images/profile.png'
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

export default class Grievance extends React.Component {

    constructor(props) {
        super(props)
        this.state = {

        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }
    onBackPress = () => {
        this.props.navigation.navigate("Landing");
        return true;
    };
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
    }

    render() {
        return (
            <Container>
                <NHeader onPressHome={() => this.props.navigation.navigate('Landing')} name="Grievance" />
                <StatusBar barStyle="light-content" backgroundColor="#0EA697" />
                    <Content style={{ backgroundColor: "rgb(40, 153, 87)" }}>
                    <ImageBackground source={BodyBackgroundimage} style={{width: screenWidth, height: screenHeight-60}}>
                        <ImageBackground imageStyle={{position:'absolute',marginTop:screenHeight*0.35}} source={Backgroundimage} resizeMode='contain' style={{width: screenWidth, height: screenHeight-50}}>
                            <Image source={Logo}  style={{marginTop:40,marginBottom:30,alignSelf:'center'}}/>
                            <Text style={{borderTopColor:'#fff',borderWidth:2,padding:15,textAlign:'center',marginLeft:20,marginRight:20,marginBottom:30,color:'#fff',borderBottomColor:'#fff',borderLeftWidth:0,borderRightWidth:0,fontWeight:'bold'}}>Online grievance redressal mechanism</Text>

                            <View style={{flexDirection:'row',justifyContent:'space-between',paddingLeft:40,paddingRight:40}}>
                                <TouchableOpacity style={{borderRadius:10,backgroundColor:'#fff',padding:20,width:screenWidth/3}} onPress={() => this.props.navigation.navigate("Webview",{webview:'https://www.nandankanan.org/mobile/grievance/grievance-lodge.php'})}>
                                    <Image  style={{width:50,height:50,alignSelf:'center'}}
                                        source={MessageImg}
                                    />
                                    <Text numberOfLines={2} style={{textAlign:'center',fontWeight:'bold',color:'green'}}>Lodge Grievance</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{borderRadius:10,backgroundColor:'#fff',padding:20,width:screenWidth/3}} onPress={() => this.props.navigation.navigate("Webview",{webview:'https://www.nandankanan.org/mobile/grievance/grievance-status.php'})}>
                                <Image style={{width:50,height:50,alignSelf:'center'}}
                                        source={ProfileImg}
                                    />
                                    <Text numberOfLines={2} style={{textAlign:'center',fontWeight:'bold',color:'green'}}>View Status</Text>
                                </TouchableOpacity>
                            </View>
                        </ImageBackground>
                        </ImageBackground>
                    </Content>
            </Container>
        )
    }
}
