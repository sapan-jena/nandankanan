import React from 'react'
import {Linking,TouchableOpacity} from 'react-native'
import {Header,Text, Icon,Left,Right} from 'native-base';

export default function NHeader(props){
    return (
        <Header hasTabs style={{backgroundColor:'#ffffff',borderBottomColor:"#ddd",borderBottomWidth:2}}>
                 <Left style={{flexDirection:'row'}}>
                     <TouchableOpacity transparent onPress={() => props.onPressHome()}>
                         <Icon type="Ionicons" name='md-arrow-back' style={{ fontSize: 25, color: '#0EA697',marginRight:10 }} />
                     </TouchableOpacity>
                     <TouchableOpacity transparent>
                         <Text numberOfLines={1} style={{color:'#0EA697',marginTop:2,width:180,fontWeight:'bold'}}>{props.name.toUpperCase()}</Text>
                        {/* <Image source={Logo} style={{width:120,height:37}} /> */}
                     </TouchableOpacity>
                 </Left>

                 <Right>
                     <TouchableOpacity transparent onPress={() => props.onPressHome()}><Icon type="AntDesign" name="home" style={{color:'#0EA697',marginRight:20}}/></TouchableOpacity>
                     <TouchableOpacity transparent onPress={()=>{Linking.openURL('tel:+919438918584');}}><Icon type="Feather" name="phone-call" style={{color:'#0EA697',marginRight:20}}/></TouchableOpacity>
                     {/* <TouchableOpacity transparent onPress={()=>{Linking.openURL('mailto:sapanjena@gmial.com');}}><Icon type="AntDesign" name="mail" style={{color:' rgb(40, 153, 87)'}}/></TouchableOpacity> */}
                 </Right>
            </Header>
    )

}