import {createDrawerNavigator,createStackNavigator} from 'react-navigation';
import React, { Component } from 'react';
import { Platform,Dimensions } from 'react-native';
import Splash from '../screens/Splash';
import Landing from '../screens/Landing';
import ZooTime from '../screens/ZooTime';
import ZooMap from '../screens/ZooMap';
import Tender from '../screens/Tender';
import News from '../screens/News';
import Amenities from '../screens/Amenities';
import Garden from '../screens/Garden';
import Gallery from '../screens/Gallery';
import Contact from '../screens/Contact';
import Webview from '../screens/Webview';
import ZooAnimalDetails from '../screens/ZooAnimalDetails'; 
import BotanicalGarden from '../screens/BotanicalGarden';
import ZoologicalPark from '../screens/ZoologicalPark';
import ErrorPage from '../screens/ErrorPage'
import Grievance from '../screens/Grievance'

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

// const Drawer=({
//   //drawer screens
//     // SearchDestination:{screen:SearchDestination},
// });
// const NavigationMenu = createDrawerNavigator(
//   Drawer,
//   {
//     initialRouteName:'Landing',
//     drawerWidth: screenWidth - (Platform.OS === 'android' ? 56 : (screenWidth >414 ? 500 : 64)),
//     contentComponent: props => <Sidebar {...props} routes={Drawer}/>,
//     drawerPosition:'left',
//   });

  const StackNav = createStackNavigator(
  {
    Splash:{screen:Splash},
    Landing:{screen:Landing},
    ZooTime:{screen:ZooTime},
    ZooMap:{screen:ZooMap},
    Tender:{screen:Tender},
    News:{screen:News},
    Amenities:{screen:Amenities},
    Garden:{screen:Garden},
    Contact:{screen:Contact},
    Gallery:{screen:Gallery},
    BotanicalGarden:{screen:BotanicalGarden},
    ZooAnimalDetails:{screen:ZooAnimalDetails},
    ZoologicalPark:{screen:ZoologicalPark},
    Grievance:{screen:Grievance},
    Webview:{screen:Webview},
    ErrorPage:{screen:ErrorPage}
  },
{headerMode:'none'});

export default StackNav;
