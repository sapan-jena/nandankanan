const baseurl = "https://www.nandankanan.org";

/**
 * Fetch api function to call apis
 * 
 * @param {string} url URL for the api to retrive data from server
 * @param {str} method Relevent api method
 * @param {object} body JSON body to be passed 
 * @param {function} successCallback Callback function when there is a response from the server
 * @param {function} errorCallback Callback function when there is some error encountered
 */

const fetchApi = (url,method,body=null,successCallback,errorCallback)=>{
    
    if (method==="get" && body!==null){
        errorCallback("GET request does not support body")
        return null
    }

    if (method==="get"){
        fetch(url,{
            method:method,
            headers:{"Content-type":"application/json"}
        })
        .then(response=>{
            return response.json()
        })
        .then(responseJson=>{
            successCallback(responseJson) 
        }).catch(error=>{
            errorCallback(error)
        })
    }else{
        fetch(url,{
            method:method,
            body:body,
            headers:{"Content-type":"application/json"}
        })
        .then(response=>{
            console.log(response)
            return response.json()
        })
        .then(responseJson=>{
            console.log(responseJson)
            successCallback(responseJson)
        }).catch(error=>{
            errorCallback(error)
        })
    }
}

/**
 * Get tender details from the nandankanan server
 * 
 * @param {function} successCallback Called when executed successfully
 * @param {function} errorCallback Called when error occurs
 */

const getTenderDetails = (successCallback,errorCallback) => {
    fetchApi(`${baseurl}/tender-api.php`,"get",null,successCallback,errorCallback);
}

/**
 * Get News details from the nandankanan server
 * 
 * @param {function} successCallback Called when executed successfully
 * @param {function} errorCallback Called when error occurs
 */

const getNewsDetails = (successCallback,errorCallback) => {
    fetchApi(`${baseurl}/news-api.php`,"get",null,successCallback,errorCallback);
}

/**
 * Get Photo Gallery details from the nandankanan server
 * 
 * @param {function} successCallback Called when executed successfully
 * @param {function} errorCallback Called when error occurs
 */

const getPhotoGalleryDetails = (successCallback,errorCallback) => {
    fetchApi(`${baseurl}/photo-gallery-api.php?cid=b6d767d2f8ed5d21a44b0e5886680cb9`,"get",null,successCallback,errorCallback);
}

/**
 * Get contact details from the nandankanan server
 * 
 * @param {function} successCallback Called when executed successfully
 * @param {function} errorCallback Called when error occurs
 */

const getContactDetails = (successCallback,errorCallback) => {
    fetchApi(`${baseurl}/contact-api.php`,"get",null,successCallback,errorCallback);
}

export {getTenderDetails,getNewsDetails,getPhotoGalleryDetails,getContactDetails};
