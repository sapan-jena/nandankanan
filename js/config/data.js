// Zoo Time start

function ZooTime (header,month1,time1,month2,time2,footer){
    return{header,month1,time1,month2,time2,footer}
}
const ZooTimeData = [
    ZooTime('Zoological Park',' April to September','(07.30 hrs. to 17.30 hrs.)',' October to March','(08.00 hrs. to 17.00 hrs.)','Zoological Park remains closed on every Monday'),

    ZooTime('Botanical Garden',' April to September','(07.30 hrs. to 17.30 hrs.)',' October to March','(08.00 hrs. to 17.00 hrs.)','Botanical Garden remains closed on every Monday'),
]

// Zoo Time end

// Garden start

function GardenData (header,body){
    return{header,body}
}
const GardenDetails= [
    GardenData('Infrastructure','The Botanical Garden covers 24 different satellite gardens and parks spread all over undulating landscapes mixed with natural vegetation and meadows. The important facilities available in the Botanical Garden are F.R.H, picnic pindies, interpretation centre etc.'),
    GardenData('Glass House','A spacious glass house of 2013 sqft. has been developed for housing succulent plants. A variety of succulant adds beauty to the Glass House. One can enjoy blooming ornamental plants from the period from November to March.'),
    GardenData('Cacti House','A 1080 sqft. polyhouse houses important species of cactii plants. A rich collection of more than 1200 cacti are available in the Cacti House.'),
    GardenData('Bonsai','A rich collection of more than 300 numbers of bonsai of 55 varieties are available.'),
    GardenData('Philodendron House','More than 2000 numbers of 5 varieties of philodendron are available.'),
    GardenData('Dry Garden','A garden of xerophytes plants has been developed over an area of 0.65 Acre in front of Glass House. The entire garden has been designed with loose stone work to give it exclusive look of dry zone which attracts the visitors.'),
    GardenData('Green House','A state of art green house of 5200 Sq ft. houses the cultivars of 60 species of indoor plants like Aglaonema, Philodendron, Calathea, Cordyline, Diffenbachia, Spathophyllum, Peperomia, Monstera, Hemigraphis etc...'),
    GardenData('Butterfly Park','Butterfly Park, an interesting option among visitors has been developed over an area of 36,590 sqft. The garden has a unique landscape with 6400 butterfly dependant plants. The garden has one cascading waterfall with 170 sqft. flowing water channel, fountains and artistic bridges. One can sight more than 81 species of butterfly here. The visitor will get the fragrance of beautiful flowers and feel the beauty of flying jewels of nature. The newly created Butterfly Park was opened to the visitors in February, 2014.This Butterfly Park also contain a butterfly interpretation centre. The Centre spreads over an area of 800 sqft has been developed near the Butterfly Park. The newly opened centre has colourful pictorial display boards to disseminate information on butterfly to the visitors. The main objective of the centre is to provide latest information on various aspects of butterfly. A short documentary about butterfly is an added attraction. The Interpretation centre was opened to the visitors in March, 2015.'),
    GardenData('Rosarium','It is one of the richest collections of 1200 Rose plants belonging to 55 varieties grown over an area of 1.5 acres of land. The garden is liked by the visitors most and has maximum foot flow during the winter.'),
    GardenData('Orchid House','The largest orchid house of the State spreads over an area of 5000 sqft. and displays more than 1000 orchids of 57 varieties belonging to 37 species. One can enjoy the orchid in bloom round the year displaying various colour of nature.'),
    GardenData('Medicinal Garden','A well laid medicinal garden spread over an area of 2.26 acres which displays of rich collection of 225 species of trees, shrubs and herbs.'),
    GardenData('Japanese Garden','A state of art Japanese Garden in Tsukiyama-niwa style with flowing streams and other typical features is a star attraction. It covers an area of 0.50 acres.'),
    GardenData('Mughal Garden','Mughal Garden covering an area of 1.01 ha. has a cascade of flowing water and flowering plants both seasonal and perennial on both side give a look of gardens of Mughal era. The garden is liked mostly by the young people and has maximum foot flow during winter.'),
    GardenData('Evolution Garden','The newly constucted Evolution Garden covering an area of 0.77 ha. gives complete information about the evolution system of plant kingdom. The one way path takes a visitor from lower plant groups like Algae, fungi, Bryophyta, Pteridophyta to evolution of large plant groups like Gymnosperm and Angiosperms. The concept of such garden is very informative for the students and researchers.'),
    GardenData('Heritage Garden','The Heritage Garden covering an area of 1.2 acres is situated adjoining to the Evolution garden with natural rock and old Banyan trees. A Tulsi (Ocimum sanctum) garden containing six varieties of Tulsi plant with models of monk, live size cow and tribal hut are the attraction of the garden.'),
    GardenData('Landscape Garden','It is a model landscaping having different elements like lawn, trees and shrubs spread over an area of 1.6 acres. 1000 trees of different species have been planted to maintain the rhythm.'),
    GardenData('Palm Garden','Palm garden is an amazing site to see varieties of common and rare palms. A collection of 29 varieties of beautiful palms have been planted over an area of 15000 sqft .One designed water body along with 15000 sqft grass carpet has been developed inside the Palm Garden for the public.'),
    GardenData('Bougainvillea Garden','One Bougainvillea garden has been developed over an area of 21400 sqft. inside the State Botanical Garden. More than 1000 Bougainvillea plants of different colours (8 varieties) have been displayed in the garden. A series of creepers raised on the back of the garden adds beauty to the park.'),
    GardenData('Arboretum','The Arboretum spread over an area of 4 ha. has been established with collection of more than 234 species (from 49 families) found in the eastern Zone forests. A minimum of 10 nos. of each species has been planted with signage displayed. A network of path has been made through the plantation which will be an ideal walk way for botanization for the trainees and students.'),
    GardenData('Hibiscus Garden','A garden has been developed over an area of 11000 sqft. located in between glass house and Palm garden. More than 130 numbers of plants of 27 varieties have been planted in the garden.'),
    GardenData('Carnivorous Plant Garden','The garden established inside the state botanical garden covering an area of 800 sqft. is intended to exhibit carnivorous plants. Nandankanan is 1st of its kind to display carnivorous Plants. The garden has been developed initially to accommodate 4 species of carnivorous plants with provision of naturalistic landscape. It was opened to the visitors in March 2016.'),
    GardenData("Children's Park",'A niche for the kids has been created within the natural setting by providing swings, merry-go-round, sliding chute etc. The gallery located nearby filled with ornamental plants enhances beauty of the Park.'),
    GardenData('Buddha Park','The park has been developed over an area of 12000 sqft. located by the scenic beauty of Kanjia Lake. It features a beautiful Buddha Statue as its centre place. Rain shelter, sit outs and ornamental flower beds are other attractions.'),
    GardenData('Artificial Zoo','Models of different animals are used to explain the ecological niche with an objective to generate awareness among the children.'),
    GardenData('Vision','To achieve the distinction of a progressive Botanical Garden.'),
]

// Garden end

// Map Data start

function ZoologicalMapHeader(title,latitude,longitude,url){
    return{title,latitude,longitude,url}
}

const ZoologicalMapDetails = [
    ZoologicalMapHeader('Nandankanan Zoological Park',20.396385,85.825924,require('../../images/markers/zoological/nandankanan-gate.png')),
    ZoologicalMapHeader('Children park',20.3953796,85.8240816,require('../../images/markers/botanical/children-park.png')),
    ZoologicalMapHeader('Bear zone',20.3945658,85.8236913,require('../../images/markers/zoological/bear.png')),
    ZoologicalMapHeader('Leopard',20.3939803,85.8229782,require('../../images/markers/zoological/leopard.png')),
    ZoologicalMapHeader('Tiger',20.3945157,85.8222889,require('../../images/markers/zoological/tiger.png')),
    ZoologicalMapHeader('Lion',20.3943908,85.8210444,require('../../images/markers/zoological/lion.png')),
    ZoologicalMapHeader('Black Tiger',20.3943054,85.8201356,require('../../images/markers/zoological/tiger.png')),
    ZoologicalMapHeader('White Tiger',20.3944112,85.8197735,require('../../images/markers/zoological/tiger.png')),
    ZoologicalMapHeader('Nandan, Tiger',20.3944048,85.8197506,require('../../images/markers/zoological/tiger.png')),
    ZoologicalMapHeader('Elephant',20.3935684,85.8185071,require('../../images/markers/zoological/elephant.png')),
    ZoologicalMapHeader('Free Public Toilet',20.394888,85.818656,require('../../images/markers/zoological/toilet.png')),
    ZoologicalMapHeader('Asiatic wild dog',20.3949142,85.8180713,require('../../images/markers/zoological/asiatic-wild-dog.png')),
    ZoologicalMapHeader('Drinking water',20.3950056,85.8178903,require('../../images/markers/zoological/drinking-water.png')),
    ZoologicalMapHeader('White Tiger Cage',20.3951113,85.8169301,require('../../images/markers/zoological/tiger.png')),
    ZoologicalMapHeader('Tiger safari',20.3953853,85.814694,require('../../images/markers/zoological/safari.png')),
    ZoologicalMapHeader('Giraffe',20.3960793,85.8145773,require('../../images/markers/zoological/girraffe.png')),
    ZoologicalMapHeader('Drinking water Near Giraffe Cage',20.3965709,85.814755,require('../../images/markers/zoological/drinking-water.png')),
    ZoologicalMapHeader('Lion & bear safari',20.3967398,85.8147235,require('../../images/markers/zoological/bear.png')),
    ZoologicalMapHeader('Free Public Toilet',20.39631,85.818947,require('../../images/markers/zoological/toilet.png')),
    ZoologicalMapHeader('Magger Crocodile',20.395705,85.8187378,require('../../images/markers/zoological/crocodile.png')),
    ZoologicalMapHeader('Gharial',20.3957157,85.8194352,require('../../images/markers/zoological/crocodile.png')),
    ZoologicalMapHeader('Gayal',20.3959321,85.8198105,require('../../images/markers/zoological/nilgai.png')),
    ZoologicalMapHeader('Reptile park',20.3963652,85.8203969,require('../../images/markers/zoological/reptile-park.png')),
    ZoologicalMapHeader('Free Public Toilet',20.3961779,85.8205897,require('../../images/markers/zoological/toilet.png')),
    ZoologicalMapHeader('Refreshment center',20.3962403,85.820715,require('../../images/markers/zoological/restaurant.png')),
    ZoologicalMapHeader('Lion-tailed macaque',20.3962832,85.821177,require('../../images/markers/zoological/lion-tailed-macaque.png')),
    ZoologicalMapHeader('Deer park',20.3961979,85.8224509,require('../../images/markers/zoological/deer.png')),
    ZoologicalMapHeader('Ostrich',20.3960657,85.8230788,require('../../images/markers/zoological/ostrich.png')),
    ZoologicalMapHeader('Bird aviatory',20.3957572,85.8223613,require('../../images/markers/zoological/bird-aviatory.png')),
    ZoologicalMapHeader('Drinking water',20.3959542,85.8237707,require('../../images/markers/zoological/drinking-water.png')),
    ZoologicalMapHeader('BOV point',20.3959542,85.8237707,require('../../images/markers/zoological/bov.png')),
    ZoologicalMapHeader('Entrance',20.3959542,85.8237707,require('../../images/markers/zoological/nandankanan-gate.png')),
    ZoologicalMapHeader('Walk through aviary',20.395660,85.822101,require('../../images/markers/zoological/bird-aviatory.png')),
    ZoologicalMapHeader('Refreshment Center',20.396457,85.820856,require('../../images/markers/zoological/restaurant.png')),
    ZoologicalMapHeader('BOV Stop near Refreshment Center',20.396175,85.820463,require('../../images/markers/zoological/bov.png')),
    ZoologicalMapHeader('Drinking Water Near refreshment Center',20.396036,85.820471,require('../../images/markers/zoological/drinking-water.png')),
    ZoologicalMapHeader('Safari Ticket Counter',20.396201,85.820153,require('../../images/markers/zoological/ticket-counter.png')),
    ZoologicalMapHeader('Drinking Water Near Boating',20.397796,85.820207,require('../../images/markers/zoological/drinking-water.png')),
    ZoologicalMapHeader('Boating',20.398507,85.820416,require('../../images/markers/zoological/boating.png')),
    ZoologicalMapHeader('Shelter Near Kanjia Lake',20.398458,85.821256,require('../../images/markers/zoological/shelter.png')),
    ZoologicalMapHeader('BOV Stop near hippopotamus',20.396684,85.820623,require('../../images/markers/zoological/bov.png')),
    ZoologicalMapHeader('hippopotamus',20.396824,85.820602,require('../../images/markers/zoological/hippopotamus.png')),
    ZoologicalMapHeader('Rehsus Macaque',20.396592,85.821099,require('../../images/markers/zoological/lion-tailed-macaque.png')),
    ZoologicalMapHeader('Chimpanzee',20.396755,85.821138,require('../../images/markers/zoological/chimpanzee.png')),
    ZoologicalMapHeader('Shelter Near Chimpanzee',20.396740,85.821131,require('../../images/markers/zoological/shelter.png')),
    ZoologicalMapHeader('Spotted Deer',20.396206,85.822350,require('../../images/markers/zoological/spotted-dear.png')),
    ZoologicalMapHeader('Nilgai',20.396171,85.822828,require('../../images/markers/zoological/nilgai.png')),
]

function BotanicalMapHeader(title,latitude,longitude,url){
    return{title,latitude,longitude,url}
}
const BotanicalMapDetails = [
    BotanicalMapHeader('Botanical garden',20.402099,85.827182,require('../../images/markers/botanical/garden.png')),
    BotanicalMapHeader('Rest House 1',20.402096,85.826049,require('../../images/markers/botanical/rest-house.png')),
    BotanicalMapHeader('Palm Garden',20.403332,85.822955,require('../../images/markers/botanical/garden.png')),
    BotanicalMapHeader('Drinking Water Near Palm Garden',20.403342,85.822665,require('../../images/markers/zoological/drinking-water.png')),
    BotanicalMapHeader('Glass House',20.403514,85.822111,require('../../images/markers/botanical/rest-house.png')),
    BotanicalMapHeader('Rest House 2',20.403616,85.819576,require('../../images/markers/botanical/rest-house.png')),
    BotanicalMapHeader('Butterfly Garden',20.403940,85.823330,require('../../images/markers/botanical/garden.png')),
    BotanicalMapHeader('Office',20.403587,85.824838,require('../../images/markers/botanical/picnic-shed.png')),
    BotanicalMapHeader('Ladies Toilet',20.403978,85.824729,require('../../images/markers/botanical/toilet.png')),
    BotanicalMapHeader('Picnic Shades',20.405447,85.824671,require('../../images/markers/botanical/picnic-shed.png')),
    BotanicalMapHeader('Picnic Pindis',20.405647,85.823958,require('../../images/markers/botanical/picnic-shed.png')),
    BotanicalMapHeader('public Toilet Near Picnic Spot',20.406071,85.824346,require('../../images/markers/botanical/toilet.png')),
    BotanicalMapHeader('Picnic Pindis',20.405881,85.825453,require('../../images/markers/botanical/picnic-shed.png')),
    BotanicalMapHeader('OTDC Snacks Bar',20.401970,85.824247,require('../../images/markers/botanical/restaurant.png')),
    BotanicalMapHeader('Boating Ticket Counter',20.401830,85.824390,require('../../images/markers/botanical/ticket-counter.png')),
    BotanicalMapHeader('Public Toilet Near OTDC Snacks Bar',20.401997,85.823763,require('../../images/markers/botanical/toilet.png')),
    BotanicalMapHeader('Evolution Garden',20.402020,85.823231,require('../../images/markers/botanical/garden.png')),
    BotanicalMapHeader('Holiday Cottage',20.402597,85.820443,require('../../images/markers/botanical/rest-house.png')),
    BotanicalMapHeader('Green House',20.402662,85.819946,require('../../images/markers/botanical/garden.png')),
    BotanicalMapHeader("Children's Park",20.402720,85.819755,require('../../images/markers/botanical/children-park.png')),
    BotanicalMapHeader('Artificial Zoo',20.403213,85.818669,require('../../images/markers/botanical/garden.png')),
    BotanicalMapHeader('Budha Park',20.402983,85.818336,require('../../images/markers/botanical/garden.png')),
    BotanicalMapHeader('Medicinal garden',20.402722,85.817295,require('../../images/markers/botanical/garden.png')),
    BotanicalMapHeader('Japanese Park',20.402740,85.817844,require('../../images/markers/botanical/garden.png')),
    BotanicalMapHeader('Orchid House',20.402864,85.815793,require('../../images/markers/botanical/garden.png')),
    BotanicalMapHeader('Mughal Garden',20.403379,85.815190,require('../../images/markers/botanical/garden.png')),
    BotanicalMapHeader('Special Picnic Shade near Mughal Garden',20.403206,85.815222,require('../../images/markers/botanical/picnic-shed.png')),
    BotanicalMapHeader('Special Picnic Shade near Orchid House',20.402866,85.816274,require('../../images/markers/botanical/picnic-shed.png')),
    BotanicalMapHeader('Lake View Road',20.401606,85.825703,require('../../images/markers/zoological/boating.png')),
]

// Map Data end

export {ZooTimeData,GardenDetails,ZoologicalMapDetails,BotanicalMapDetails}